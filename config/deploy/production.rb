server "161.202.13.46", 
  roles: %w{app web db},
  primary: true,
  user: 'deployer', # Username for server
  password: 'Johnny55'

set :ssh_options, {
  forward_agent: true
  #verbose: :debug
}

set :deploy_to, "/var/www/prelaunchr"
set :branch, 'master'
set :rails_env, "production"
set :keep_releases, 5
